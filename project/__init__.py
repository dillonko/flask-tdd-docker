# project/__init__.py

from flask import Flask, jsonify
from flask_restplus import Resource, Api

# Instance of the App
app = Flask(__name__)

api = Api(app)

class Ping(Resource):
    def get(self):
        return {
            'status': 'success',
            'message': 'pong!'
        }

api.add_resource(Ping, '/ping')